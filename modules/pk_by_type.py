import pandas 

def _get_types(pokemons_df):
    """
    this function gets the amount of pokemons per types
    :param pokemons_df: a pandas dataframe containing all the pokemons
    :return: a pandas series containing the pokemons types and the number of
    pokemons that have them
    """
    df_aux = pokemons_df.explode("types")
    df_aux = df_aux.groupby("types").size()
    df_aux = df_aux.sort_values()
    return df_aux


def _get_u_types(pokemons_df):
    """
    this function gets the amount of pokemons per unique types
    :param pokemons_df: a pandas dataframe containing all the pokemons
    :return: a pandas series containing the pokemons unique types and 
    the number of pokemons that have them
    """
    df_aux = pokemons_df.copy()
    df_aux['types'] = df_aux['types'].apply(lambda x: '/'.join(x))
    df_aux = df_aux.groupby("types").size()
    df_aux = df_aux.sort_values(ascending=False)
    return df_aux
    
