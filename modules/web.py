from bs4 import BeautifulSoup
import requests
import pandas as pd

URL = "https://bulbapedia.bulbagarden.net/wiki/List_of_Pok%C3%A9mon_by_Kanto_Pok%C3%A9dex_number"


def _get_page_text(url: str):
    response = requests.get(url)
    if response.status_code == 200:
        content = response.text
        return content
    return None


def _get_pokemons(content: str):
    result_df_l = []
    soup = BeautifulSoup(content, "html.parser")
    tables = soup.find_all("table")[1:6]
    for table in tables:
        tags = table.find_all("tr", {"style": "background:#FFF"})
        for tag in tags:
            dex = tag.find_all("td")
            kdex = dex[0].text[:-1]
            ndex = dex[1].text[:-1]
            name = tag.find("a")["title"]
            span_tags = tag.find_all("span")
            img = "https:" + tag.find("img")["src"]
            types = [pokemon_type.text for pokemon_type in span_tags]
            structure_df = [kdex, ndex, name, img, types]
            result_df_l.append(structure_df)
    result_df = pd.DataFrame(result_df_l,columns=["kdex", "ndex", "name", "img", "types"])
    return result_df