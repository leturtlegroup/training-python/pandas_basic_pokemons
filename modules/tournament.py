import json
import pandas
import numpy as np 


def _build_teams(pokemons_df, n_teams : int):
    """
    this function buids teams of 6 pokemons each
    :param pokemons_df: a pandas dataframe containing all the pokemons
    :param n_teams: an int for the number of teams to build
    :return: an array of pandas dataframes, each dataframe contains six pokemons with
    their name, types and attack
    """
    teams = []
    for i in range(n_teams):
        team = pokemons_df.sample(n=6)
        team = team[['name','types']]
        team['attacks'] = np.random.randint(1, 100, team.shape[0])
        teams.append(team)
    return teams


def _read_weakness(file:str):
    with open(file, "r") as table:
        weak = json.load(table)
    return weak


def _get_df(df_interaction):
    df_strong = df_interaction['Strong'].to_frame()
    df_strong = df_strong.reset_index()
    df_strong = df_strong.explode('Strong')
    df_immune = df_interaction['Immune'].to_frame()
    df_immune = df_immune.reset_index()
    df_immune = df_immune.explode('Immune')
    return df_immune,df_strong


def _interaction(team,df_interaction,interaction):
    pka = 'pa_'+interaction
    pkb = 'pb_'+interaction
    pair = team.explode('types_x').explode('types_y')
    df_interaction = df_interaction.reset_index()
    pair = pair.merge(df_interaction[['index',interaction]], left_on= 'types_x', right_on='index',how = 'inner')
    pair[pka] = pair[interaction]
    pair = pair.drop([interaction,'index'],axis=1)
    pair = pair.merge(df_interaction[['index',interaction]], left_on= 'types_y', right_on='index',how = 'inner')
    pair[pkb] = pair[interaction]
    pair = pair.drop([interaction,'index'],axis=1)
    pair = pair.explode(pka).explode(pkb)
    pair[pka] = np.where(pair[pka] == pair['types_y'], True, False)
    pair[pkb] = np.where(pair[pkb] == pair['types_x'], True, False)
    return pair


def _get_gb_interaction(pair,df_interaction,interaction):
    df_interac=_interaction(pair,df_interaction,interaction)
    gb_interac= df_interac.groupby(['name_x','attacks_x','name_y','attacks_y'])
    return gb_interac


def _interaction_result(gb_pair,interaction):
    pka = "pa_"+ interaction
    pkb = "pb_"+ interaction
    result = 0
    for fight,df_figth in gb_pair:
        if interaction == 'Immune':
            immune2b = 1 if df_figth[pka].any() else 0
            immune2a = -1 if df_figth[pkb].any() else 0
            if immune2b or immune2a:
                return immune2b + immune2a
            else:
                return 0
        strong2b = 2 if df_figth[pka].any() else 1
        strong2a = 2 if df_figth[pkb].any() else 1
        pwr_a = df_figth['attacks_x'].iloc[0]
        pwr_b = df_figth['attacks_y'].iloc[0]
        result = pwr_a * strong2b - pwr_b * strong2a
        if result == 0:
            return 0
        return result / abs(result)


def _untie(gb_interaction):
    sum_a = 0
    sum_b = 0
    for fight,df_fight in gb_interaction:
        sum_a += int(fight[1])
        sum_b += int(fight[3])
    result = sum_a -sum_b
    return result / abs(result)


def _vs(pair,df_interaction):
    interaction = 'Immune'
    gb_interaction = _get_gb_interaction(pair,df_interaction,interaction)
    winner = _interaction_result(gb_interaction,interaction)
    if winner != 0 return winner
    interaction = 'Strong'
    gb_interaction = _get_gb_interaction(pair,df_interaction,interaction)
    winner = _interaction_result(gb_interaction,interaction)
    if winner == 0 winner = _untie(gb_interaction)
    return winner



def _round(teams,df_interaction):
    lista = []
    for i in range(int(len(teams)/2)):
        team_a = teams[i*2].reset_index().drop(['index'], axis=1)
        team_b = teams[i*2+1].reset_index().drop(['index'], axis=1)
        team_a['key']=team_a.index
        team_b['key']=team_b.index
        pair = team_a.merge(team_b, on= 'key',how = 'inner').drop(['key'], axis=1)
        winner = _vs(pair,df_interaction)
        team_a = team_a.drop(['key'], axis=1)
        team_b = team_b.drop(['key'], axis=1)
        if winner >0 :
            lista.append(team_a)
        if winner <0 :
            lista.append(team_b)
    return lista


def _tournament(teams,df_interaction):
    winner = []
    for i in range(4):
        if len(winner) ==0:
            winner = _round(teams,df_interaction)
        else:
            winner = _round(winner,df_interaction)
        if len(winner) == 1:
            return winner
