import modules.web as web
import modules.pk_by_type as pktype
import modules.tournament as t
import pandas as pd


if __name__ == "__main__":
    pokemons_df = web._get_pokemons(web._get_page_text(web.URL))
    types = pktype._get_types(pokemons_df)
    u_types = pktype._get_u_types(pokemons_df)
    n_teams=16
    teams = t._build_teams(pokemons_df,n_teams)
    interactions = t._read_weakness("data/weakness.json")
    df_interaction = pd.DataFrame.from_dict(interactions,orient='index',columns=['Weak','Strong','Immune'])
    winner = t._tournament(teams,df_interaction)
    print(winner)


